browser.pageAction.onClicked.addListener(function (tab) {
  browser.tabs.sendMessage(tab.id, true);
});

browser.runtime.onMessage.addListener(function (tweetInfo) {
  let id = tweetInfo.id;
  tweetInfo.id = undefined;
  let storageObject = {}
  storageObject[id] = tweetInfo;
  browser.storage.local.set(storageObject);
});

browser.browserAction.onClicked.addListener(function (_) {
  browser.tabs.create({
    active: true,
    url: "/tweets.html"
  }).then(function (newTab) {
    browser.tabs.insertCSS(newTab.id, {file: "tweets.css"});
    browser.storage.local.get(null).then(function (tweets) {
      browser.tabs.executeScript(newTab.id, {file: "/jquery.js"}).then(function () {
        browser.tabs.executeScript(newTab.id, {file: "/tweets.js"}).then(
          function () {
            browser.tabs.sendMessage(newTab.id, JSON.stringify(tweets));
          }
        );
      });
    });
  });
});
