// TODO: h/t Rex: https://twitter.com/ThatRextream/status/1151111694946570240
// bring back color gamut
// edit button
// "copy link to tweet" in drop-down
// fix video buffering
// smaller buttons

// see https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver#Example_usage


// Run the option callback if the option is set to true in storage
function setupOption(optionID, callback) {
 chrome.storage.local.get(optionID, function (setting) {
   // Assume options that are not yet stored as keys are set to true
   if (Object.keys(setting).length == 0 || setting[optionID]) {
    callback();
   }
 });
}

function makeLinkLink(anchor, suffix, observer) {
  url = anchor.attr("href");
  if (!url.endsWith(suffix)) anchor.attr("href", url+suffix);
  anchor.click(function (e) {
    e.preventDefault();
    window.open(anchor.attr("href"), "_self"); // h/t vdbuilder https://stackoverflow.com/a/8454535/948073
    $(startObserver);
  });
}

function swap(x, y) {
  let xc = x.clone();
  let yc = y.clone();
  x.replaceWith(yc);
  y.replaceWith(xc);
}

/*
function swapRandomPair(jq) {
  let len = jq.length;
  let i = Math.floor(Math.random()*len);
  let j;
  do {
    j = Math.floor(Math.random()*len);
  } while (i == j);
  swap(jq.eq(i), jq.eq(j));
}
*/

optionCallbacks = {
  "mark-promo": function () {
    /*
    $(".r-1d4mawv").each(function () {
      $(this).closest(".r-qklmqi").css("border", "12px solid red");
    });
    */
    $("svg.r-1d4mawv:not('.hacked')").each(function () {
      $(this).closest(".r-qklmqi").css("border", "12px solid red");
      /*
      $(this).addClass("hacked");
      if ($(this).text().startsWith("Promoted")) {
        $(this).closest("article").css("border", "12px solid red");
      }
      */
    });
  },
  "save-deleted": function () { // h/t David M. Perry https://twitter.com/Lollardfish/status/1183800440460369922
    $(".r-11cpok1").click(function () {
      window.tweet = $(this).closest(".r-1mi0q7o"); // kludge
      window.statusUrl = tweet.find("a[href*='status']").attr("href");
      window.epoch = tweet.find("time").attr("datetime");
    });
    let deleteButton = $(".r-daml9f").first().parent();
    if (deleteButton) {
      deleteButton.click(function () {
        browser.runtime.sendMessage({
          id: window.statusUrl.split("/").pop(),
          html: window.tweet.html(),
          epoch: new Date(window.epoch).getTime()
        });
      });
    }
  },
  "fix-embedded": function () {
    $("span[title^='http']").each(function () {
      $(this).wrap("<a>").parent().attr("href", $(this).attr("title"));
      $(this).removeAttr("title");
    });
    $("[role='blockquote']:not(.hacked)").each(function () {
      $(this).addClass("hacked").css("background-color", "#fc8");
      let text = $(this).find("[lang]").text();
      $(this).find("time").wrap("<a>").parent().attr("href", `https://twitter.com/search?q=${text}`);
      $(this).find("span").filter(function () {
        return $(this).text().match(/^#\w+$/);
      }).each(function () {
        // TODO wrap a tag around hashtag
      });
    });
    // $("[href*='status']").css("border", "12px solid #80f");
  },
  "add-followers-to-list": function () {
  },
  "virgin-tweets": function () {
    // $(".r-1mdbhws:not(:has('.r-1k6nrdp, .r-1wgg2b2'))").css("background", "pink");
    // $("article:not(:has('.r-1k6nrdp'))").css("background", "pink");
    /*
    $(".r-1mdbhws").not(function () {
      return $(this).has(".r-1k6nrdp").not(function () {
        return $(this).text() != "";
      });
    }).closest("article").css("background", "pink");
    */
    $(".r-1mdbhws").each(function () {
      if ($(this).text() == "") {
        $(this).closest("article").css("background", "pink");
      }
    });
  },
  "with-replies": function () {
    $("section a, aside a").each(function () {
      url = $(this).attr("href");
      if (url && url.match(/^\/\w+$/)) {
        makeLinkLink($(this), "/with_replies", observer);
      }
    });
  },
  "whats-happening": function () {
    $(".r-vmopo1").closest(".r-1ifxtd0").remove();
  },
  "latest-tweets": function () {
    $("a[href^='/hashtag'], a[href^='/search']").each(function () {
      if ($(this).hasClass("hacked")) return;
      makeLinkLink($(this), "&f=live", observer);
    });
    $(".r-vmopo1:not(.hacked)").each(function () {
      $(this).addClass("hacked");
      let text = $(this).text();
      let url = text[0] == "#" ? `https://twitter.com/hashtag/${text.substring(1)}?f=live` : `https://twitter.com/search?q=${text}&f=live`;
      $(this).wrap("<a>").parent().attr("href", url);
    });
  },
  "raw-unicode": function() {
    // console.log("raw unicode callback checking in");
    $("img[src*='emoji'").replaceWith(function () {
      return $(this).attr("alt");
    });
  },
  "hacked-search": function () {
    $("form[role='search']").each(function () {
      if ($(this).hasClass("hacked")) return;
      if ($(this).hasClass("visited")) return;
      $(this).addClass("visited");
      let hackedSearch = $(this).clone();
      hackedSearch.addClass("hacked");
      let hackedInput = hackedSearch.find("[data-testid='SearchBox_Search_Input']");
      hackedInput.attr("placeholder", "Search Latest Tweets");
      hackedSearch.submit(function (e) {
        e.preventDefault();
        let query = hackedInput.val();
        let url = `https://twitter.com/search?q=${query}&f=live&qf=off`;
        window.open(url, "_self");
        $(startObserver);
      });
      hackedSearch.keyup(function (e) {
        e.preventDefault();
        if (e.which == 13) hackedSearch.submit();
      });
      $(this).after(hackedSearch); // actually hackedSearch goes after $(this)...
    });
  },
  "untco-links": function () {
    $("a[title^='http']").each(function () {
      // console.log(`${$(this).attr("href")} --> ${$(this).attr("title")}`);
      $(this).attr("href", $(this).attr("title"));
      $(this).removeAttr("title");
      $(this).css("background", "yellow");
    });
  },
  "search-instead": function () {
    $("[rel='preconnect']:not('.hacked')").each(function () {
      $(this).addClass("hacked");
      if ($(this).attr("href").startsWith("//")) {
        let parent = $(this).parent();
        // let title = parent.find("span").first().text();
        let title = parent.find(".r-18jsvk2").text();
        let query = title.trim().replace(/\W+/g, "+");
        let tld = $(this).attr("href").slice(2);
        let site = encodeURIComponent(tld);
        let url = `https://duckduckgo.com/?q=${query}+site%3A${site}`;
        parent.find("a").attr("href", url);
        parent.css("background", "#fc8");
        $(this).find("span").first().css("background", "yellow");
      }
    });
  },
  /*
  "search-instead": function () {
    $(".r-1mi0q7o").each(function () {
      if ($(this).text() != "") { // kludge
        let title = $(this).children().first().text();
        let query = title.split(/\W+/).join(" ").trim().replace(/ /g, "+");
        let tld = $(this).children().last().text();
        let site = encodeURIComponent(tld);
        let url = `https://duckduckgo.com/?q=${query}+site%3A${site}`;
        $(this).closest("a").attr("href", url);
      }
    });
  },
  */
  "latest-timeline": function () {
    $.get("https://api.twitter.com/2/timeline/home_latest.json?include_profile_interstitial_type=1&include_blocking=1&include_blocked_by=1&include_followed_by=1&include_want_retweets=1&include_mute_edge=1&include_can_dm=1&include_can_media_tag=1&skip_status=1&cards_platform=Web-12&include_cards=1&include_composer_source=true&include_ext_alt_text=true&include_reply_count=1&tweet_mode=extended&include_entities=true&include_user_entities=true&include_ext_media_color=true&include_ext_media_availability=true&send_error_codes=true&earned=1&count=20&ext=mediaStats%2ChighlightedLabel%2CcameraMoment", function (data) {
    }, function () {
      console.error("error in request");
    });
  },
  "autoplaying-media": function () {
  },
  "extreme-prejudice": function () {
  },
  "in-case": function () {
  },
  "while-away": function () {
  },
  "show-contact": function () {
  },
  // New:
  "prefer-small": function () { // h/t Chuck Baggett https://twitter.com/ChuckBaggett/status/958156853455843328
  }
  // Twitter extension idea: https://twitter.com/pookleblinky/status/959487077632135169
};

// create an observer instance
var observer = new MutationObserver(observerCallback);

function observerCallback(mutations) {

  // Set up turned-on features:
  Object.keys(optionCallbacks).forEach(function (optionID) {
    setupOption(optionID, optionCallbacks[optionID]);
  });
}


function startObserver() {
  // select the target node
  var target = $("main").get(0);

  // configuration of the observer:
  var config = { childList: true, subtree: true };

  // pass in the target node, as well as the observer options
  if (target) observer.observe(target, config);

  // later, you can stop observing
  // observer.disconnect();
}

// override button for if page load event misfires or something:
browser.runtime.onMessage.addListener(function (request) {
  $(startObserver);
  $(observerCallback); // Count initial page load as if it's a mutation
});

$(startObserver);
$(observerCallback); // Count initial page load as if it's a mutation

