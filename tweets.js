browser.runtime.onMessage.addListener(function (tweetsJSON) {
  let tweets = JSON.parse(tweetsJSON);
  let tweetContainer = $("<div>");
  tweetContainer.attr("id", "tweet-container");
  // return;
  Object.keys(tweets).forEach(function (key) {
    let tweet = tweets[key];
    let tweetElement = $("<div>");
    tweetElement.attr("data-tweet-id", tweet.id);
    tweetElement.addClass("tweet");
    let dateElement = $("<div>");
    dateElement.attr("data-tweet-epoch-second", tweet.epoch);
    dateElement.text((new Date(tweet.epoch)).toLocaleString());
    tweetElement.append(dateElement);
    let contentElement = $("<div>");
    contentElement.html(tweet.html); // HTML comes from twitter.com
    tweetElement.append(contentElement);
    tweetContainer.append(tweetElement);
  });
  $("body").append(tweetContainer);
});
